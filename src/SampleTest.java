public class SampleTest {
    @Test
    public void testAdditions() {
        int result = 4;
        int operation = 2 + 2;
        assertEquals(result, operation, "2 + 2 = 4?");
    }
    @Test
    public void testNullObject() {
        assertNull(null, "NULL?");
        assertNotNull(new Object(), "NOT NULL?");
    }
    @Test
    public void testTheSameObject() {
        assertSame("JAVA 8", "JAVA 8");
        assertNotSame(12.0, 12);
    }
    @Test
    public void testBoolean() {
        assertTrue(12 == 12, "12 == 12?");
        assertFalse(("I don’t like JAVA").equals("I will pass exam"), "Is it true?");
    }
    @Test
    public void testFail() {
// fail("Show my error");
    }
}